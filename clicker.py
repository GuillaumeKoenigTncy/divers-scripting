from pynput.mouse import Button, Controller
import keyboard

from time import sleep

mouse = Controller()
print ("Current position: " + str(mouse.position))
print ("5")
sleep(1)
print ("4")
sleep(1)
print ("3")
sleep(1)
print ("2")
sleep(1)
print ("1")
sleep(1)
mouse.position = (450, 350)
print ("Current position: " + str(mouse.position))
print("Begin click - press s key to stop")

while True:
    sleep(0.005)
    if keyboard.is_pressed('S'):
        print('Stop clicking !')
        break
    else:
        mouse.click(Button.left, 1)
